FROM nginx:stable-alpine
RUN mkdir /etc/nginx/default-locations
RUN sed -i '#^}#iinclude /etc/nginx/default-locations/\*.conf;' /etc/nginx/conf.d/default.conf
ADD etc/nginx/conf.d/log_format.conf /etc/nginx/conf.d/AAA-log_format.conf
ADD etc/nginx/default-locations/logging.conf /etc/nginx/default-locations/

RUN nginx -t
